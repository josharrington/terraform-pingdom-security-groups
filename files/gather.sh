#!/bin/sh
curl -s https://my.pingdom.com/probes/ipv4 | jq --raw-input --slurp 'split("\n") | map(select(. != "")) | map(. + "/32") | { cidr_blocks: .} | {result: (. | tojson)}'
