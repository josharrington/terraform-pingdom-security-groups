# Pingdom Probe Security Group Module

Creates security groups to allow Pingdom to monitor your service. Automatically scales the number of security groups based on the number of Pingdom source IPs and ports. Requires `jq` and `curl` to fetch the Pingdom IPs

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| aws | n/a |
| external | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| name | Name prefix of the security group | `string` | n/a | yes |
| ports | A list of ports | `list(any)` | n/a | yes |
| rules\_per\_sg | How many rules per security group to create. 60 is the current max allowed by AWS. | `number` | `60` | no |
| tags | A map of tags to apply to all resources. | `map(string)` | `{}` | no |
| vpc\_id | The ID of the VPC to create the security groups in. | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| security\_group\_ids | n/a |
