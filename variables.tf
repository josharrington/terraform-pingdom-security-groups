variable "tags" {
  type        = map(string)
  default     = {}
  description = "A map of tags to apply to all resources."
}

variable "ports" {
  description = "A list of ports "
  type        = list(any)
}

variable "name" {
  type        = string
  description = "Name prefix of the security group"
}

variable "vpc_id" {
  description = "The ID of the VPC to create the security groups in."
}

variable "rules_per_sg" {
  description = "How many rules per security group to create. 60 is the current max allowed by AWS."
  default     = 60
}