locals {
  cidr_blocks         = jsondecode(data.external.pingdom.result["result"])["cidr_blocks"]
  chunked_cidr_blocks = chunklist(local.cidr_blocks, var.rules_per_sg)
  num_sg              = length(local.chunked_cidr_blocks) * length(var.ports)
  num_rules           = length(var.ports) * length(local.cidr_blocks)
}

data "external" "pingdom" {
  program = ["${path.module}/files/gather.sh"]
}

resource "aws_security_group" "this" {
  count       = local.num_sg
  name        = "${var.name}-${count.index}"
  description = "Allow Pingdom Port ${var.ports[count.index % length(var.ports)]}"
  vpc_id      = var.vpc_id
  tags        = merge(var.tags, { "Name" : "${var.name}-${count.index}" })

  ingress {
    from_port   = var.ports[count.index % length(var.ports)]
    to_port     = var.ports[count.index % length(var.ports)]
    description = "Allow Pingdom Port ${var.ports[count.index % length(var.ports)]}"
    protocol    = "tcp"
    cidr_blocks = local.chunked_cidr_blocks[count.index % length(local.chunked_cidr_blocks)]
  }
}
